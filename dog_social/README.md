# Dog Social App

An Interview Task project.

## Getting Started

This app is built with pure Flutter code and the design was provided by the team.

I made Post Request to this [link](https://hookb.in/pzQ7aK2YoKURPnrrPdZY) because the link provided was returning an error message.

I made the UI pixel perfect and it looks good.

### Recommendations
If i am given more time, 
    - I would complete the screens once the UIs are available
    - I will make all parts of the UI interactive and responsive as well.
    - I will also make more Request to the backend and pull in data gotten.
    - I will also write more tests.

## Thank You! 😎

### ERINOLA Jeremiah, Oluwaseun.
